//AQUI EMPIEZA LA ACCION
$(document).ready(function(){

	cargaPrincipal();
    cargaSelects();

});

function cargaPrincipal(e) {

	$.ajax({
		url: 'php/dashboard/carga-principal.php',
		contentType: "application/json",
        context: document.body,
        success: function(result) {
        	$('.counterOrd').html(result[0]);
        	$('.counterInv').html(result[1]);
        	$('.counterPro').html(result[2]);
        	$('.counterDep').html(result[3]);
        },

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

};

function cargaSelects(e) {

	var $ordSelect = $('#ordSelect');
	var $proSelect = $('#proSelect');
	var $depSelect = $('#depSelect');

	$.ajax({
		type: 'post',
		url: 'php/dashboard/carga-select.php',
		data: ({
	      source: 0
	    }),
	    contentType: "application/json",
        context: document.body,
        success: function(result) {
        	$.each(result.a, function(i, object) {
				$.each(object, function(key, value) {
					if (key == 'id') {
						optionValue = value;
					} else if (key == 'noo') {
						htmlNom = value;
					} else if (key == 'idp') {
						htmlPro = value;
					}
				});
				$ordSelect.append('<option value=' + optionValue + '>' + htmlNom + ' - ' + htmlPro + '</option>');
			});
			$ordSelect.formSelect();
			$.each(result.c, function(i, object) {
				$.each(object, function(key, value) {
					if (key == 'id') {
						optionValue = value;
					} else if (key == 'nom') {
						htmlNom = value;
					}
				});
				$proSelect.append('<option value=' + optionValue + '>' + htmlNom + '</option>');
			});
			$proSelect.formSelect();
			$.each(result.d, function(i, object) {
				$.each(object, function(key, value) {
					if (key == 'id') {
						optionValue = value;
					} else if (key == 'nom') {
						htmlNom = value;
					}
				});
				$depSelect.append('<option value=' + optionValue + '>' + htmlNom + '</option>');
			});
			$depSelect.formSelect();
        },

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

};

$('.selectReport').on('change',function(){
	var source = $(this).attr('val');

	$.ajax({
		type: 'post',
		url: 'php/dashboard/carga-reporte.php',
		data: ({
	      source: source
	    }),
		dataType: "html",
		context: document.body,
        success: function(result) {
        	
        },
		error: function(xhr, tst, err) {
			console.log(err);
		}
	});
});