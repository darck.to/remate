<?php	

	//GENERADOR DE CADENAS ALEATORIAS
	function generateRandomString($length = 10) {
	    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	//CAMBIAMOS PESO DE LAS IMAGENES
	function compress($source, $destination, $quality) {
		$info = getimagesize($source);
		if ($info['mime'] == 'image/jpeg') 
		  $image = imagecreatefromjpeg($source);
		elseif ($info['mime'] == 'image/gif') 
		  $image = imagecreatefromgif($source);
		elseif ($info['mime'] == 'image/png') 
		  $image = imagecreatefrompng($source);
		imagejpeg($image, $destination, $quality);
		return $destination;
	}

	function nombreProveedor($id){
		include __DIR__ . ('/abre_conexion.php');
		$sql = $mysqli->query("SELECT nom FROM pro_rm WHERE id = '$id'");
		if ($sql->num_rows > 0) {
			$row = $sql->fetch_assoc();
			$nombre = $row['nom'];
		}
		return $nombre;
		include __DIR__ . ('/cierra_conexion.php');
	}

	function nombreAutor($src,$id){
		include __DIR__ . ('/abre_conexion.php');
		$sql = $mysqli->query("SELECT nom FROM $src WHERE id = '$id'");
		if ($sql->num_rows > 0) {
			$row = $sql->fetch_assoc();
			$nombre = $row['nom'];
		}
		return $nombre;
		include __DIR__ . ('/cierra_conexion.php');
	}

?>