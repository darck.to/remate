
<!-- jQuery -->
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>

<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript" src="js/materialize.min.js"></script>

<!--Modal-->
<script type="text/javascript" src="js/jquery.modal.min.js"></script>

<!--DataTable-->
<script type="text/javascript" src="js/datatables.js"></script>
<script type="text/javascript" src="js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="js/buttons.flash.min.js"></script>
<script type="text/javascript" src="js/buttons.html5.min.js"></script>
<script type="text/javascript" src="js/buttons.print.min.js"></script>
<script type="text/javascript" src="js/jszip.min.js"></script>
<script type="text/javascript" src="js/pdfmake.min.js"></script>
<script type="text/javascript" src="js/vfs_fonts.js"></script>

<!-- Load -->
<script type="text/javaScript" src="scripts/onLoad.js"></script>
<script type="text/javaScript" src="init/login.js"></script>