<?php
	
	header('Content-type: application/json');

	include_once('../../func/abre_conexion.php');

	$fuente = $_POST['source'];
	$bdd = $fuente."_rm";

	$resultados = array();

	if ($sql = $mysqli->query("SELECT id, nom FROM $bdd ORDER BY id ASC")) {
		if ($sql->num_rows > 0) {
			while ($row = $sql->fetch_assoc()) {
				$resultados[] = array(
					'id' => $row['id'],
					'nom' => $row['nom'],
				);
			}
		}

	} else {
		echo("</br>Error: " . mysqli_error($mysqli));
	}

	print json_encode($resultados);

	include('../../func/cierra_conexion.php');

?>