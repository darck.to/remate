<?php
	
	header('Content-type: application/json');

	include_once('../../func/abre_conexion.php');
	include_once('../../func/functions.php');
	$resultados = array();

	if ($sql = $mysqli->query("SELECT id, nom, idp, can, ida, idd, pre FROM inv_rm ORDER BY id ASC")) {
		if ($sql->num_rows > 0) {
			while ($row = $sql->fetch_assoc()) {
				$resultados[] = array(
					'nom' => $row['nom'],
			        'idp' => nombreProveedor($row['idp']),
			        'can' => $row['can'],
			        'ida' => nombreAutor("alm_rm",$row['ida']),
			        'idd' => nombreAutor("dep_rm",$row['idd']),
			        'pre' => $row['pre'],
					'id' => $row['id'],
					'ipd' => $row['idp'],
					'iad' => $row['ida'],
			        'ide' => $row['idd'],
				);
			}
			$resultados = array_map('array_values', $resultados);
			$resultados = array_values($resultados);
		}

	} else {
		echo("</br>Error: " . mysqli_error($mysqli));
	}

	print json_encode($resultados);

	include('../../func/cierra_conexion.php');

?>