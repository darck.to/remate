$('#form-proveedor').submit(function(event) {
	event.preventDefault();

	var formNombre = $(this).attr('name');
	var formData = new FormData(document.getElementById("form-proveedor"));
	var formMethod = $(this).attr('method');
	var rutaScrtip = $(this).attr('action');

	var request = $.ajax({
		url: rutaScrtip,
		method: formMethod,
		data: formData,
		contentType: false,
		processData: false,
		dataType: "html"
	});

	// handle the responses
	request.done(function(data) {
		M.toast({html: data});
		$.modal.close();
		location.reload();

	})
	request.fail(function(jqXHR, textStatus) {
		console.log(textStatus);
	})
	request.always(function(data) {
		// clear the form
		$('form[name="' + formNombre + '"]').trigger('reset');
	});

});