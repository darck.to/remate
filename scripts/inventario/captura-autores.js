$('#form-autores').submit(function(event) {
	event.preventDefault();

	var formNombre = $(this).attr('name');
	var formData = new FormData(document.getElementById("form-autores"));
	var formMethod = $(this).attr('method');
	var rutaScrtip = $(this).attr('action');

	var request = $.ajax({
		url: rutaScrtip,
		method: formMethod,
		data: formData,
		contentType: false,
		processData: false,
		dataType: "html"
	});

	// handle the responses
	request.done(function(data) {
		M.toast({html: data});
		$.modal.close();
		//location.reload();

	})
	request.fail(function(jqXHR, textStatus) {
		console.log(textStatus);
	})
	request.always(function(data) {
		// clear the form
		$('form[name="' + formNombre + '"]').trigger('reset');
	});

});

$('.clickSearch').focusin(function(){
	var fuente = $(this).attr('val');
	$('#tags-'+fuente).css('display','none');
	//MUESTRA EL LISTADO DE AUTORES REGISTRADOS
	$.ajax({
		type: 'post',
		url: 'php/inventario/carga-autores.php',
		data: ({
	      source: fuente
	    }),
		dataType: "html",
		context: document.body,
        success: function(result) {
        	$('#tags-'+fuente).html('');
        	$.each(JSON.parse(result), function(index, prov) {
                $('#tags-'+fuente).append("<li class='p-a' val=" + prov.id + ">" + prov.nom + "</li>");
            });
        },
		error: function(xhr, tst, err) {
			console.log(err);
		}
	});
});