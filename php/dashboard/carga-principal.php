<?php
	
	header('Content-type: application/json');

	include_once('../../func/abre_conexion.php');
	include_once('../../func/functions.php');
	$resultados = array();
	//ORDENES DE COMPRA
	if ($sql = $mysqli->query("SELECT COUNT(id) FROM ord_rm")) {
		if ($sql->num_rows > 0) {
			while ($row = $sql->fetch_assoc()) {
				$resultados[] = $row['COUNT(id)'];
			}
		}

	} else {
		echo("</br>Error: " . mysqli_error($mysqli));
	}
	//INVENTARIOS
	if ($sql = $mysqli->query("SELECT COUNT(id) FROM inv_rm")) {
		if ($sql->num_rows > 0) {
			while ($row = $sql->fetch_assoc()) {
				$resultados[] = $row['COUNT(id)'];
			}
		}

	} else {
		echo("</br>Error: " . mysqli_error($mysqli));
	}
	//PROVEEDORES
	if ($sql = $mysqli->query("SELECT COUNT(id) FROM pro_rm")) {
		if ($sql->num_rows > 0) {
			while ($row = $sql->fetch_assoc()) {
				$resultados[] = $row['COUNT(id)'];
			}
		}

	} else {
		echo("</br>Error: " . mysqli_error($mysqli));
	}
	//DEPARTAMENTOS
	if ($sql = $mysqli->query("SELECT COUNT(id) FROM dep_rm")) {
		if ($sql->num_rows > 0) {
			while ($row = $sql->fetch_assoc()) {
				$resultados[] = $row['COUNT(id)'];
			}
		}

	} else {
		echo("</br>Error: " . mysqli_error($mysqli));
	}

	print json_encode($resultados);

	include('../../func/cierra_conexion.php');

?>