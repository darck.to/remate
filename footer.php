<footer class="page-footer blue-grey darken-1 grey-text text-darken-4">

	<div class="grey-text text-darken-4 footer-copyright">
		
		<div class="container white-text">
			Jerez de Garcia Salinas, Zacatecas
			<i class="right">Administración 2018-2021</i>
		</div>

	</div>

</footer>