<!--MODAL CAPTURA INVENTARIO-->
<div class="row">
	<div class="col s12 center-align">
		<h5 class="blue-grey-text text-darken-3"><i class="fas fa-user-plus"></i>&nbsp;Orden</h5>
		<div class="section"></div>
		<form id="form-orden" class="modal-form p-r" name="form_orden" action="php/orden/captura-orden.php" method="post" autocomplete="off">
			<input class="grey lighten-2 numOrd" type="text" name="noo" placeholder="N&uacute;mero de Orden" required>
			<input class="grey lighten-2 clickSearch proOrd" type="text" name="pro" placeholder="Proveedor" val="pro" required>
			<input class="idpro" type="hidden" name="idp">
			<ul id="tags-pro" class="tags"></ul>
			<input class="grey lighten-2 preOrd" type="text" name="pre" placeholder="Precio">
			<input class="idOrd" type="hidden" name="id">
			<div class="section"></div>
			<button class="btn" type="submit" name="guardar">Guardar</button>
		</form>
	</div>
</div>

<script type="text/javascript" src="scripts/orden/captura-orden.js"></script>