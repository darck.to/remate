$('#form-inventario').submit(function(event) {
	event.preventDefault();

	var formNombre = $(this).attr('name');
	var formData = new FormData(document.getElementById("form-inventario"));
	var formMethod = $(this).attr('method');
	var rutaScrtip = $(this).attr('action');

	var request = $.ajax({
		url: rutaScrtip,
		method: formMethod,
		data: formData,
		contentType: false,
		processData: false,
		dataType: "html"
	});

	// handle the responses
	request.done(function(data) {
		M.toast({html: data});
		$.modal.close();
		location.reload();

	})
	request.fail(function(jqXHR, textStatus) {
		console.log(textStatus);
	})
	request.always(function(data) {
		// clear the form
		$('form[name="' + formNombre + '"]').trigger('reset');
	});

});

$('.clickSearch').focusin(function(){
	var fuente = $(this).attr('val');
	$('#tags-'+fuente).css('display','none');
	//MUESTRA EL LISTADO DE AUTORES REGISTRADOS
	$.ajax({
		type: 'post',
		url: 'php/inventario/carga-autores.php',
		data: ({
	      source: fuente
	    }),
		dataType: "html",
		context: document.body,
        success: function(result) {
        	$('#tags-'+fuente).html('');
        	$.each(JSON.parse(result), function(index, prov) {
                $('#tags-'+fuente).append("<li class='tagAutor' val=" + prov.id + ">" + prov.nom + "</li>");
				
				//CLICK LISTADO DE AUTORES
				$('.tagAutor').on('click', function(){
					var autor = $(this).html();
					var id = $(this).attr('val')
					$('.'+fuente+'Inv').val(autor);
					$('.id'+fuente).attr('value',id);
					$('.'+fuente+'Inv').prop('readonly', true);
					$('.'+fuente+'Inv').addClass('lockBackground');
					$('#tags-'+fuente).css('display','none');
					$('.'+fuente+'Inv').removeClass('lighten-2');
					$('.'+fuente+'Inv').addClass('lighten-4');
				});

            });
        },
		error: function(xhr, tst, err) {
			console.log(err);
		}
	});
});

$('.clickSearch').focusin(function(){
	$('.tags').css('display','none');
});

$( ".clickSearch" ).dblclick(function() {
	var fuente = $(this).attr('val');
  	$(this).prop('readonly', false);
  	$('.'+fuente+'Inv').removeClass('lighten-4');
	$('.'+fuente+'Inv').addClass('lighten-2');
	$('.'+fuente+'Inv').removeClass('lockBackground');
});

//FILTRO PARA INPUT DE AUTORES
$('.clickSearch').keyup(function() {
	var fuente = $(this).attr('val');
    var input, filter, contenedor, p, i;
    input = $(this);    
    filter = input.val().toUpperCase();
    contenedor = $('#tags-'+fuente+' > li')
    for (i = 0; i < contenedor.length; i++) {
        p = contenedor[i];
        if (p.innerHTML.toUpperCase().indexOf(filter) > -1) {
            $('#tags-'+fuente).css('display','block');
            contenedor[i].style.display = "";
        } else {
            contenedor[i].style.display = "none";
        }
    }

});