<div class="navbar-fixed">

	<nav>
			
		<div class="nav-wrapper blue-grey darken-3 z-depth-0">

			<div class="container">

				<a href="." class="brand-logo">
					<h5>Panel</h5>
				</a>

				<a href="#" data-target="mobile-nav" class="sidenav-trigger"><i class="material-icons">menu</i></a>

				<ul id="nav-mobile" class="right hide-on-med-and-down">
					
					<li><a href="."><i class="material-icons">home</i></a></li>

					<?php
					
						session_start();
						if (isset($_SESSION['usuario_nombre'])) {

					?>

					<li class="handed sidenav-close orange darken-2"><a href="orden.php"><div class="valign-wrapper">Orden</div></a></li>
					<li class="handed sidenav-close teal darken-2"><a href="inventario.php"><div class="valign-wrapper">Inventario</div></a></li>
					<li class="handed sidenav-close light-blue darken-2"><a href="proveedores.php"><div class="valign-wrapper">Proveedores</div></a></li>

					<?php

					} else {

					}

					?>

					<li><a class="handed" onclick="initFunction()"><i class="material-icons">vpn_key</i></a></li>

				</ul>

			</div>

		</div>

	</nav>

</div>

<ul class="sidenav grey lighten-4" id="mobile-nav">
	
	<li class="handed"><a href="."><i class="material-icons">home</i></a></li>

<?php

if (isset($_SESSION['usuario_nombre'])) {

?>

	<li class="handed sidenav-close orange darken-2"><a href="orden.php"><div class="valign-wrapper">Orden</div></a></li>
	<li class="handed sidenav-close"><a><div class="valign-wrapper">Inventario</div></a></li>
	<li class="handed sidenav-close"><a><div class="valign-wrapper">Proveedores</div></a></li>

<?php

} else {

}

?>

	<li><a class="handed sidenav-close" onclick="initFunction()"><i class="material-icons">vpn_key</i></a></li>

</ul>