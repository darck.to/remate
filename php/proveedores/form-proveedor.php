<!--MODAL CAPTURA PROVEEDOR-->
<div class="row">
	<div class="col s12 center-align">
		<h5 class="blue-grey-text text-darken-3"><i class="fas fa-user-plus"></i>&nbsp;Proveedor</h5>
		<div class="section"></div>
		<form id="form-proveedor" class="modal-form" name="form_proveedor" action="php/proveedores/captura-proveedor.php" method="post" autocomplete="off">
			<input class="grey lighten-2 nomPro" type="text" name="nom" placeholder="Nombre" required>
			<input class="grey lighten-2 telPro" type="text" name="tel" placeholder="Tel&eacute;fono">
			<input class="grey lighten-2 maiPro" type="text" name="mai" placeholder="Em@il">
			<input class="grey lighten-2 dirPro" type="text" name="dir" placeholder="Direcci&oacute;n">
			<input class="idPro" type="hidden" name="id">
			<div class="section"></div>
			<button class="btn capturaProveedor" type="submit" name="guardar">Guardar</button>
		</form>
	</div>
</div>

<script type="text/javascript" src="scripts/proveedores/captura-proveedor.js"></script>