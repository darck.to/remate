<!--MODAL CAPTURA INVENTARIO-->
<div class="row">
	<div class="col s12 center-align">
		<h5 class="blue-grey-text text-darken-3"><i class="fas fa-user-plus"></i>&nbsp;Inventario</h5>
		<div class="section"></div>
		<form id="form-inventario" class="modal-form p-r" name="form_inventario" action="php/inventario/captura-inventario.php" method="post" autocomplete="off">
			<input class="grey lighten-2 nomInv" type="text" name="nom" placeholder="Nombre" required>
			<input class="grey lighten-2 clickSearch proInv" type="text" name="pro" placeholder="Proveedor" val="pro" required>
			<input class="idpro" type="hidden" name="idp">
			<ul id="tags-pro" class="tags"></ul>
			<input class="grey lighten-2 canInv" type="number" name="can" placeholder="Cantidad" required>
			<input class="grey lighten-2 clickSearch almInv" type="text" name="alm" placeholder="Almacen" val="alm" required>
			<input class="idalm" type="hidden" name="ida">
			<ul id="tags-alm" class="tags"></ul>
			<input class="grey lighten-2 clickSearch depInv" type="text" name="dep" placeholder="Departamento" val="dep" required>
			<input class="iddep" type="hidden" name="idd">
			<input class="grey lighten-2 preInv" type="text" name="pre" placeholder="Precio">
			<input class="idInv" type="hidden" name="id">
			<ul id="tags-dep" class="tags"></ul>
			<div class="section"></div>
			<button class="btn" type="submit" name="guardar">Guardar</button>
		</form>
	</div>
</div>

<script type="text/javascript" src="scripts/inventario/captura-inventario.js"></script>