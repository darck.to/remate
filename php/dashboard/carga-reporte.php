<?php
	
	header('Content-type: application/json');

	include_once('../../func/abre_conexion.php');
	include_once('../../func/functions.php');
	$resultados = array();

	$source = $_POST['value'];

	switch ($source) {
	    case 0:
	 		//ORDENES DE COMPRA
			if ($sql = $mysqli->query("SELECT id, noo, idp, pre FROM ord_rm ORDER BY id ASC")) {
				if ($sql->num_rows > 0) {
					while ($row = $sql->fetch_assoc()) {
						$resultados[] = array(
							'noo' => $row['noo'],
					        'idp' => nombreProveedor($row['idp']),
							'ipd' => $row['idp'],
					        'pre' => $row['pre'],
							'id' => $row['id'],
						);
					}
					$resultados = array_map('array_values', $resultados);
					$resultados = array_values($resultados);
				}

			} else {
				echo("</br>Error: " . mysqli_error($mysqli));
			}
	        break;
	    case 1:
	    	//INVENTARIOS
			if ($sql = $mysqli->query("SELECT id, nom, idp, can, ida, idd, pre FROM inv_rm ORDER BY id ASC")) {
				if ($sql->num_rows > 0) {
					while ($row = $sql->fetch_assoc()) {
						$resultados[] = array(
							'nom' => $row['nom'],
					        'idp' => nombreProveedor($row['idp']),
					        'can' => $row['can'],
					        'ida' => nombreAutor("alm_rm",$row['ida']),
					        'idd' => nombreAutor("dep_rm",$row['idd']),
					        'pre' => $row['pre'],
							'id' => $row['id'],
							'ipd' => $row['idp'],
							'iad' => $row['ida'],
					        'ide' => $row['idd'],
						);
					}
					$resultados = array_map('array_values', $resultados);
					$resultados = array_values($resultados);
				}

			} else {
				echo("</br>Error: " . mysqli_error($mysqli));
			}       
	        break;
	    case 2:
	        //PROVEEDORES
			if ($sql = $mysqli->query("SELECT id, nom, tel, mai, dir FROM pro_rm ORDER BY id ASC")) {
				if ($sql->num_rows > 0) {
					while ($row = $sql->fetch_assoc()) {
						$resultados[] = array(
							'nom' => $row['nom'],
					        'tel' => $row['tel'],
					        'mai' => $row['mai'],
					        'dir' => $row['dir'],
							'id' => $row['id'],
						);
					}
					$resultados = array_map('array_values', $resultados);
					$resultados = array_values($resultados);
				}

			} else {
				echo("</br>Error: " . mysqli_error($mysqli));
			}
	        break;
	    case 3:
	        //DEPARTAMENTOS
			if ($sql = $mysqli->query("SELECT id, nom FROM dep_rm ORDER BY id ASC")) {
				if ($sql->num_rows > 0) {
					while ($row = $sql->fetch_assoc()) {
						$resultados[] = array(
							'nom' => $row['nom'],
					        'tot' => totalDepartamento($row['tel']),
							'id' => $row['id'],
						);
					}
					$resultados = array_map('array_values', $resultados);
					$resultados = array_values($resultados);
				}

			} else {
				echo("</br>Error: " . mysqli_error($mysqli));
			}
	        break;
	}

	//ENVIA RESULTADOS	
	print json_encode($resultados);

	include('../../func/cierra_conexion.php');

?>