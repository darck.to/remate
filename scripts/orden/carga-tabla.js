//AQUI EMPIEZA LA ACCION
$(document).ready(function(){

	cargaTable();

});

function cargaTable(e) {

	$.ajax({
		url: 'php/orden/carga-tabla.php',
		contentType: "application/json",
        context: document.body,
        success: function(result) {

        	//CARGAMOS TABLA
        	var table = $('#table').DataTable({

				data: result,
				dom: 'Bfrtip',
				buttons: [
			        'excel', 'pdf', 'print'
			    ],			    

			});

			//EVENTOS DE CLICK
			$('#table tbody').on('click', 'tr', function () {
				var data = table.row( this ).data();
				$('.addOrden').modal({
					showClose: false
				});
				$('.addOrden').on($.modal.AJAX_COMPLETE, function(event, modal) {
					$('.delButton').remove();
					$('.numOrd').val(data[0]);
					$('.proOrd').val(data[1]);
					$('.idpro').val(data[2]);
					$('.preOrd').val(data[3]);
					$('.idOrd').val(data[4]);
					$('.clickSearch').prop('readonly', true);
					$('.clickSearch').removeClass('lighten-2');
					$('.clickSearch').addClass('lighten-4');
					$('.clickSearch').addClass('lockBackground');
					console.log('blockeo');
					$('#form-orden').append('<a class="btn-small red darken-4 delButton">Borrar</a>');
				  	//BOTON DE BORRADO
				  	$('.delButton').on('click',function(event) {
				  		$(this).removeClass('darken-4');
				  		$(this).addClass('darken-1');
				  		$(this).html('Confirmar');
				  		$(this).addClass('confirmButton');
				  		//CONFIRMACION DE BORRADO
				  		$('.confirmButton').on('click',function(event) {
					  		$.ajax({
						        type: 'post',
								url: 'php/orden/captura-orden.php',
								data: ({
							      id: data[4],
							      del: 1
							    }),
								dataType: "html",
								context: document.body,
						        success: function(result) {
						        	M.toast({html: result});
									$.modal.close();
									location.reload();
						        },

								error: function(xhr, tst, err) {
									console.log(err);
								}
							});
				  		});
				  	})
				});
			} );

        },

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

};

$('a[data-modal]').on('click',function(event) {
	$(this).modal({
		showClose: false
	});
	$('.addOrden').on($.modal.AJAX_COMPLETE, function(event, modal) {
		$('.numOrd').val('');
		$('.proOrd').val('');
		$('.idpro').val('');
		$('.preOrd').val('');
		$('.idOrd').val('');
		$('.delButton').remove();
	  	// Your code to modify modal content goes here
	});
  	return false;
});
