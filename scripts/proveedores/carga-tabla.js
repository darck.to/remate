//AQUI EMPIEZA LA ACCION
$(document).ready(function(){

	cargaTable();

});

function cargaTable(e) {

	$.ajax({
		url: 'php/proveedores/carga-tabla.php',
		contentType: "application/json",
        context: document.body,
        success: function(result) {

        	//CARGAMOS TABLA
        	var table = $('#table').DataTable({

				data: result,
				dom: 'Bfrtip',
				buttons: [
			        'excel', 'pdf', 'print'
			    ],			    

			});

			//EVENTOS DE CLICK
			$('#table tbody').on('click', 'tr', function () {
				var data = table.row( this ).data();
				$('.addProveedor').modal({
					showClose: false
				});
				$('.addProveedor').on($.modal.AJAX_COMPLETE, function(event, modal) {
					$('.delButton').remove();
					$('.nomPro').val(data[0]);
					$('.telPro').val(data[1]);
					$('.maiPro').val(data[2]);
					$('.dirPro').val(data[3]);
					$('.idPro').val(data[4]);
					$('#form-proveedor').append('<a class="btn-small red darken-4 delButton">Borrar</a>');
				  	//BOTON DE BORRADO
				  	$('.delButton').on('click',function(event) {
				  		$(this).removeClass('darken-4');
				  		$(this).addClass('darken-1');
				  		$(this).html('Confirmar');
				  		$(this).addClass('confirmButton');
				  		//CONFIRMACION DE BORRADO
				  		$('.confirmButton').on('click',function(event) {
					  		$.ajax({
						        type: 'post',
								url: 'php/proveedores/captura-proveedor.php',
								data: ({
							      id: data[4],
							      del: 1
							    }),
								dataType: "html",
								context: document.body,
						        success: function(result) {
						        	M.toast({html: result});
									$.modal.close();
									location.reload();
						        },

								error: function(xhr, tst, err) {
									console.log(err);
								}
							});
				  		});
				  	})
				});
			} );


        },

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

};

$('a[data-modal]').on('click',function(event) {
	$(this).modal({
		showClose: false
	});
	$('.addProveedor').on($.modal.AJAX_COMPLETE, function(event, modal) {
		$('.nomPro').val('');
		$('.telPro').val('');
		$('.maiPro').val('');
		$('.dirPro').val('');
		$('.idPro').val('');
		$('.delButton').remove();
	  	// Your code to modify modal content goes here
	});
  	return false;
});
