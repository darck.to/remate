<?php
	
	header('Content-type: application/json');

	include_once('../../func/abre_conexion.php');
	$resultados = array();

	if ($sql = $mysqli->query("SELECT id, nom, tel, mai, dir FROM pro_rm ORDER BY id ASC")) {
		if ($sql->num_rows > 0) {
			while ($row = $sql->fetch_assoc()) {
				$resultados[] = array(
					'nom' => $row['nom'],
			        'tel' => $row['tel'],
			        'mai' => $row['mai'],
			        'dir' => $row['dir'],
					'id' => $row['id'],
				);
			}
			$resultados = array_map('array_values', $resultados);
			$resultados = array_values($resultados);
		}

	} else {
		echo("</br>Error: " . mysqli_error($mysqli));
	}

	print json_encode($resultados);

	include('../../func/cierra_conexion.php');

?>