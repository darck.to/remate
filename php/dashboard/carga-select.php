<?php
	
	header('Content-type: application/json');

	include_once('../../func/abre_conexion.php');
	include_once('../../func/functions.php');
	$resultados = array();

	//ORDENES DE COMPRA
	if ($sql = $mysqli->query("SELECT id, noo, idp, pre FROM ord_rm ORDER BY id ASC")) {
		if ($sql->num_rows > 0) {
			while ($row = $sql->fetch_assoc()) {
				$resultados['a'][] = array(
					'id' => $row['id'],
					'noo' => $row['noo'],
			        'idp' => nombreProveedor($row['idp']),
				);
			}
			//$resultados = array_map('array_values', $resultados);
			//$resultados = array_values($resultados);
		}

	} else {
		echo("</br>Error: " . mysqli_error($mysqli));
	}
  
	//PROVEEDORES
	if ($sql = $mysqli->query("SELECT id, nom, tel, mai, dir FROM pro_rm ORDER BY id ASC")) {
		if ($sql->num_rows > 0) {
			while ($row = $sql->fetch_assoc()) {
				$resultados['c'][] = array(
					'id' => $row['id'],
					'nom' => $row['nom'],
				);
			}
			//$resultados = array_map('array_values', $resultados);
			//$resultados = array_values($resultados);
		}

	} else {
		echo("</br>Error: " . mysqli_error($mysqli));
	}
	        
    //DEPARTAMENTOS
	if ($sql = $mysqli->query("SELECT id, nom FROM dep_rm ORDER BY id ASC")) {
		if ($sql->num_rows > 0) {
			while ($row = $sql->fetch_assoc()) {
				$resultados['d'][] = array(
					'id' => $row['id'],
					'nom' => $row['nom'],
				);
			}
			//$resultados = array_map('array_values', $resultados);
			//$resultados = array_values($resultados);
		}

	} else {
		echo("</br>Error: " . mysqli_error($mysqli));
	}

	//ENVIA RESULTADOS	
	print json_encode($resultados);

	include('../../func/cierra_conexion.php');

?>