//AQUI EMPIEZA LA ACCION
$(document).ready(function(){

	cargaTable();

});

function cargaTable(e) {

	$.ajax({
		url: 'php/inventario/carga-tabla.php',
		contentType: "application/json",
        context: document.body,
        success: function(result) {

        	//CARGAMOS TABLA
        	var table = $('#table').DataTable({

				data: result,
				dom: 'Bfrtip',
				buttons: [
			        'excel', 'pdf', 'print'
			    ],			    

			});

			//EVENTOS DE CLICK
			$('#table tbody').on('click', 'tr', function () {
				var data = table.row( this ).data();
				$('.addInventario').modal({
					showClose: false
				});
				$('.addInventario').on($.modal.AJAX_COMPLETE, function(event, modal) {
					$('.delButton').remove();
					$('.nomInv').val(data[0]);
					$('.proInv').val(data[1]);
					$('.idpro').val(data[7]);
					$('.canInv').val(data[2]);
					$('.almInv').val(data[3]);
					$('.idalm').val(data[8]);
					$('.depInv').val(data[4]);
					$('.iddep').val(data[9]);
					$('.preInv').val(data[5]);
					$('.idInv').val(data[6]);
					$('.clickSearch').prop('readonly', true);
					$('.clickSearch').removeClass('lighten-2');
					$('.clickSearch').addClass('lighten-4');
					$('.clickSearch').addClass('lockBackground');
					console.log('blockeo');
					$('#form-inventario').append('<a class="btn-small red darken-4 delButton">Borrar</a>');
				  	//BOTON DE BORRADO
				  	$('.delButton').on('click',function(event) {
				  		$(this).removeClass('darken-4');
				  		$(this).addClass('darken-1');
				  		$(this).html('Confirmar');
				  		$(this).addClass('confirmButton');
				  		//CONFIRMACION DE BORRADO
				  		$('.confirmButton').on('click',function(event) {
					  		$.ajax({
						        type: 'post',
								url: 'php/inventario/captura-inventario.php',
								data: ({
							      id: data[6],
							      del: 1
							    }),
								dataType: "html",
								context: document.body,
						        success: function(result) {
						        	M.toast({html: result});
									$.modal.close();
									location.reload();
						        },

								error: function(xhr, tst, err) {
									console.log(err);
								}
							});
				  		});
				  	})
				});
			} );

        },

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

};

$('a[data-modal]').on('click',function(event) {
	$(this).modal({
		showClose: false
	});
	$('.addInventario').on($.modal.AJAX_COMPLETE, function(event, modal) {
		$('.nomInv').val('');
		$('.proInv').val('');
		$('.canInv').val('');
		$('.almInv').val('');
		$('.depInv').val('');
		$('.idInv').val('');
		$('.delButton').remove();
	});

	$('.autorTags').on($.modal.AJAX_COMPLETE, function(event, modal) {
		$('.floatTags').remove();
		$('.modal').append('<div class="floatTags"><ul class="ulTags"></ul></div>');
		//CARGA LOS TAGS CREADOS
	  	var fuente = $(this).attr('val');
		$('.floatTags').css('display','block');
		//MUESTRA EL LISTADO DE AUTORES REGISTRADOS
		$.ajax({
			type: 'post',
			url: 'php/inventario/carga-autores.php',
			data: ({
		      source: fuente
		    }),
			dataType: "html",
			context: document.body,
	        success: function(result) {
	        	$('.ulTags').html('');
	        	$.each(JSON.parse(result), function(index, prov) {
	                $('.ulTags').append("<li class='p-r'>" + prov.nom + "<i class='material-icons tiny p-a deleteAutor' val=" + prov.id + ">cancel</i></li>");
	            });
                $('.deleteAutor').on('click',function(event) {
			  		var id = $(this).attr('val');
			  		$(this).addClass('red-text');
			  		$(this).addClass('confirmDelete');
			  		//CONFIRMACION DE BORRADO
			  		$('.confirmDelete').on('click',function(event) {
				  		$.ajax({
					        type: 'post',
							url: 'php/inventario/captura-autores.php',
							data: ({
						      id: id,
						      source: fuente,
						      del: 1
						    }),
							dataType: "html",
							context: document.body,
					        success: function(result) {
					        	M.toast({html: result});
								$.modal.close();
								$('.floatTags').remove();
								location.reload();
					        },

							error: function(xhr, tst, err) {
								console.log(err);
							}
						});
			  		});
			  	})
	        },
			error: function(xhr, tst, err) {
				console.log(err);
			}
		});
	});

  	return false;
});
