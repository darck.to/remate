<?php
	$fuente = $_GET['fuente'];
	if ($fuente == "alm") {
		$titulo = "Almac&eacute;n";
	} else {
		$titulo = "Departamentos";
	}
?>
<!--MODAL CAPTURA ALMACEN-->
<div class="row">
	<div class="col s12 center-align">
		<h5 class="blue-grey-text text-darken-3"><i class="fas fa-building"></i></i>&nbsp;<?php echo $titulo;?></h5>
		<div class="section"></div>
		<form id="form-autores" class="modal-form p-r" name="form_autores" action="php/inventario/captura-autores.php" method="post" autocomplete="off">
			<input class="grey lighten-2 clickSearch" type="text" name="nom" placeholder="Nombre" required>
			<input type="hidden" name="source" value="<?php echo $fuente;?>">
			<input type="hidden" name="id">
			<div class="section"></div>
			<button class="btn" type="submit" name="guardar">Guardar</button>
		</form>
	</div>
</div>

<script type="text/javascript" src="scripts/inventario/captura-autores.js"></script>