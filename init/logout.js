function logOut() {

  $.ajax({
    cache: 'false',
    url: "keyup/logout.php",
    context: document.body,
    success: function(response) {
      $("body").html(response);
      window.location.href = (".");
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  });

}