<?php
	
	header('Content-type: application/json');

	include_once('../../func/abre_conexion.php');
	include_once('../../func/functions.php');
	$resultados = array();

	if ($sql = $mysqli->query("SELECT id, noo, idp, pre FROM ord_rm ORDER BY id ASC")) {
		if ($sql->num_rows > 0) {
			while ($row = $sql->fetch_assoc()) {
				$resultados[] = array(
					'noo' => $row['noo'],
			        'idp' => nombreProveedor($row['idp']),
					'ipd' => $row['idp'],
			        'pre' => $row['pre'],
					'id' => $row['id'],
				);
			}
			$resultados = array_map('array_values', $resultados);
			$resultados = array_values($resultados);
		}

	} else {
		echo("</br>Error: " . mysqli_error($mysqli));
	}

	print json_encode($resultados);

	include('../../func/cierra_conexion.php');

?>