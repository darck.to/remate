$('#form-orden').submit(function(event) {
	event.preventDefault();

	var formNombre = $(this).attr('name');
	var formData = new FormData(document.getElementById("form-orden"));
	var formMethod = $(this).attr('method');
	var rutaScrtip = $(this).attr('action');

	var request = $.ajax({
		url: rutaScrtip,
		method: formMethod,
		data: formData,
		contentType: false,
		processData: false,
		dataType: "html"
	});

	// handle the responses
	request.done(function(data) {
		M.toast({html: data});
		$.modal.close();
		location.reload();

	})
	request.fail(function(jqXHR, textStatus) {
		console.log(textStatus);
	})
	request.always(function(data) {
		// clear the form
		$('form[name="' + formNombre + '"]').trigger('reset');
	});

});

$('.clickSearch').focusin(function(){
	var fuente = $(this).attr('val');
	$('#tags-'+fuente).css('display','none');
	//MUESTRA EL LISTADO DE ORDEN REGISTRADOS
	$.ajax({
		type: 'post',
		url: 'php/orden/carga-orden.php',
		data: ({
	      source: fuente
	    }),
		dataType: "html",
		context: document.body,
        success: function(result) {
        	$('#tags-'+fuente).html('');
        	$.each(JSON.parse(result), function(index, prov) {
                $('#tags-'+fuente).append("<li class='tagAutor' val=" + prov.id + ">" + prov.nom + "</li>");
				
				//CLICK LISTADO DE ORDEN
				$('.tagAutor').on('click', function(){
					var autor = $(this).html();
					var id = $(this).attr('val')
					$('.'+fuente+'Ord').val(autor);
					$('.id'+fuente).attr('value',id);
					$('.'+fuente+'Ord').prop('readonly', true);
					$('#tags-'+fuente).css('display','none');
					$('.'+fuente+'Ord').removeClass('lighten-2');
					$('.'+fuente+'Ord').addClass('lighten-4');
				});

            });
        },
		error: function(xhr, tst, err) {
			console.log(err);
		}
	});
});

$('.clickSearch').focusin(function(){
	$('.tags').css('display','none');
});

$( ".clickSearch" ).dblclick(function() {
	var fuente = $(this).attr('val');
  	$(this).prop('readonly', false);
  	$('.'+fuente+'Ord').removeClass('lighten-4');
	$('.'+fuente+'Ord').addClass('lighten-2');
});

//FILTRO PARA INPUT DE ORDEN
$('.clickSearch').keyup(function() {
	var fuente = $(this).attr('val');
    var input, filter, contenedor, p, i;
    input = $(this);    
    filter = input.val().toUpperCase();
    contenedor = $('#tags-'+fuente+' > li')
    for (i = 0; i < contenedor.length; i++) {
        p = contenedor[i];
        if (p.innerHTML.toUpperCase().indexOf(filter) > -1) {
            $('#tags-'+fuente).css('display','block');
            contenedor[i].style.display = "";
        } else {
            contenedor[i].style.display = "none";
        }
    }

});